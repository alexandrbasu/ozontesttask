﻿using Moq;
using NUnit.Framework;
using OzonTestTask.Models.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OzonTestTask.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;

namespace OzonTestTask.Tests.ControllersTests.IPv4RangeControllerTests
{
    [TestFixture]
    public class WhenValidating : IPv4RangeControllerTestBase
    {
        [Test]
        [TestCase("0.0.0.1", "")]
        [TestCase("", "0.0.0.1")]
        [TestCase("0.0.0.1", null)]
        [TestCase(null, "0.0.0.1")]
        [TestCase("0.0.0.1", "0.0.")]
        [TestCase("0.0.", "0.0.0.1")]
        [TestCase("0.0.0.1", " ")]
        [TestCase(" ", "0.0.0.1")]
        [TestCase("0.0.0.1", "0.0.0.256")]
        [TestCase("0.0.0.256", "0.0.0.1")]
        public async Task ReturnsBadRequestIfInputIpHasInvalidFromat(string ipStringFrom, string ipStringTo)
        {
            var controller = GetIPv4RangeController();

            var result = await controller.GetIPRange(ipStringFrom,
                                                     ipStringTo);

            result.Result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Test]
        [TestCase("0.0.0.1", "0.0.0.0")]
        [TestCase("0.0.1.0", "0.0.0.255")]
        [TestCase("255.255.255.255", "0.0.0.0")]
        public async Task ReturnsBadRequestIfIpFromValueIsGreaterThanIpToValue(string ipStringFrom, string ipStringTo)
        {
            var controller = GetIPv4RangeController();

            var result = await controller.GetIPRange(ipStringFrom,
                                                     ipStringTo);

            result.Result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Test]
        [TestCase("0.0.0.0", "0.0.0.0")]
        [TestCase("0.0.0.0", "0.0.0.1")]
        [TestCase("0.0.0.255", "0.0.1.0")]
        [TestCase("0.0.0.0", "255.255.255.255")]
        public async Task ReturnsOkIfIpsAreCorrect(string ipStringFrom, string ipStringTo)
        {
            var controller = GetIPv4RangeController();

            var result = await controller.GetIPRange(ipStringFrom,
                                                     ipStringTo);

            result.Result.Should().BeOfType<OkObjectResult>();
        }
    }
}
