﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using OzonTestTask.Models;
using OzonTestTask.Models.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OzonTestTask.Tests.ControllersTests.IPv4RangeControllerTests
{
    [TestFixture]
    public class WhenGeneratingRange : IPv4RangeControllerTestBase
    {
        [Test]
        public async Task CallsGeneratorAsync() 
        {
            var generator = new Mock<IIPv4AsyncRangesGenerator>();
            var controller = GetIPv4RangeController(generator.Object);

            var result = await controller.GetIPRange("0.0.0.0", "0.0.0.0");

            generator.Verify(mock => mock.GenerateAsync(It.IsAny<IPv4Address>(),
                                                        It.IsAny<IPv4Address>(),
                                                        default));
        }
    }
}
