﻿using Moq;
using OzonTestTask.Controllers;
using OzonTestTask.Models.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OzonTestTask.Tests.ControllersTests.IPv4RangeControllerTests
{
    public abstract class IPv4RangeControllerTestBase
    {
        protected IPv4RangeController GetIPv4RangeController(IIPv4AsyncRangesGenerator generator = null)
        {
            return new IPv4RangeController(generator ?? new Mock<IIPv4AsyncRangesGenerator>().Object);
        }
    }
}
