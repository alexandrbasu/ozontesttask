﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using OzonTestTask.Models.Generators;

namespace OzonTestTask.Tests.ModelsTests.GeneratorsTests.IPv4RangesGeneratorTests
{
    [TestFixture]
    public class WhenGeneratingIPv4Range : IPv4GeneratorsTestBase
    {
        private IPv4RangesGenerator GetIPv4RangesGenerator()
        {
            return new IPv4RangesGenerator();
        }

        [Test]
        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(0, 255)]
        public void LastItemOfRangeIsIncluded(int intIpFrom, int intIpTo) 
        {
            var ipFrom  = GetIp(intIpFrom);
            var ipTo    = GetIp(intIpTo);
            var generator = GetIPv4RangesGenerator();

            var range = generator.Generate(ipFrom, ipTo);

            range.Last().Should().Be(ipTo);
        }

        [Test]
        [TestCase(0,  0)]
        [TestCase(0,  1)]
        [TestCase(22, 255)]
        public void FirstItemOfRangeIsIncluded(int intIpFrom, int intIpTo)
        {
            var ipFrom  = GetIp(intIpFrom);
            var ipTo    = GetIp(intIpTo);
            var generator = GetIPv4RangesGenerator();

            var range = generator.Generate(ipFrom, ipTo);

            range.First().Should().Be(ipFrom);
        }

        [Test]
        [TestCase(0,  0,  ExpectedResult = 1)]
        [TestCase(0,  1,  ExpectedResult = 2)]
        [TestCase(20, 30, ExpectedResult = 11)]
        public int RangeContainsCorrectAmountOfElements(int intIpFrom, int intIpTo)
        {
            var ipFrom  = GetIp(intIpFrom);
            var ipTo    = GetIp(intIpTo);
            var generator = GetIPv4RangesGenerator();

            var range = generator.Generate(ipFrom, ipTo);

            return range.Count();
        }
    }
}
