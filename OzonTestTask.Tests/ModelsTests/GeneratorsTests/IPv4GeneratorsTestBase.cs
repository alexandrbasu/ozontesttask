﻿using Moq;
using OzonTestTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OzonTestTask.Tests.ModelsTests.GeneratorsTests
{
    public abstract class IPv4GeneratorsTestBase
    {
        protected IPv4Address GetIp(int? ipIntRepresentation = null)
        {
            return new IPv4Address(ipIntRepresentation ?? It.IsAny<int>());
        }
    }
}
