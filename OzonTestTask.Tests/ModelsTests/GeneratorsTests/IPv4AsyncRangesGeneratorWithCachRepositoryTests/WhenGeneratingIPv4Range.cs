﻿using Moq;
using NUnit.Framework;
using OzonTestTask.Data;
using OzonTestTask.Models;
using OzonTestTask.Models.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OzonTestTask.Tests.ModelsTests.GeneratorsTests.IPv4AsyncRangesGeneratorWithCacheRepositoryTests
{
    [TestFixture]
    public class WhenGeneratingIPv4Range : IPv4GeneratorsTestBase
    {
        private IPv4AsyncRangesGeneratorWithCacheRepository GetIPv4AsyncRangesGenerator(
            IIPv4RangesAsyncRepository rangesRepository = null, 
            IIPv4RangesGenerator rangesGenerator = null)
        {
            return new IPv4AsyncRangesGeneratorWithCacheRepository(
                rangesRepository ?? new Mock<IIPv4RangesAsyncRepository>().Object, 
                rangesGenerator  ?? new Mock<IIPv4RangesGenerator>().Object);
        }

        [Test]
        public async Task TryingGetRangeFromRepository()
        {
            var repository  = new Mock<IIPv4RangesAsyncRepository>();   
            var asyncGenerator = GetIPv4AsyncRangesGenerator(repository.Object);

            var range = await asyncGenerator.GenerateAsync(It.IsAny<IPv4Address>(),
                                                           It.IsAny<IPv4Address>());

            repository.Verify(mock => mock.GetIPv4RangeAsync(It.IsAny<IPv4Address>(),
                                                             It.IsAny<IPv4Address>(),
                                                             default));
        }

        [Test]
        public async Task GeneratesNewRangeIFRangeWasntFoundInRepository()
        {
            var generator = new Mock<IIPv4RangesGenerator>();
            var asyncGenerator = GetIPv4AsyncRangesGenerator(rangesGenerator: generator.Object);

            var range = await asyncGenerator.GenerateAsync(It.IsAny<IPv4Address>(),
                                                           It.IsAny<IPv4Address>());

            generator.Verify(mock => mock.Generate(It.IsAny<IPv4Address>(),
                                                   It.IsAny<IPv4Address>()));
        }

        [Test]
        public async Task InsertsNewRangeInRepositoryIFRangeWasntFoundInRepository()
        {
            var repository = new Mock<IIPv4RangesAsyncRepository>();
            var asyncGenerator = GetIPv4AsyncRangesGenerator(repository.Object);

            var range = await asyncGenerator.GenerateAsync(It.IsAny<IPv4Address>(),
                                                           It.IsAny<IPv4Address>());

            repository.Verify(mock => mock.InsertIPv4RangeAsync(It.IsAny<IPv4Range>(),
                                                                default));
        }

        [Test]
        public async Task SavesChangesInRepositoryIFRangeWasntFoundInRepository()
        {
            var repository = new Mock<IIPv4RangesAsyncRepository>();
            var asyncGenerator = GetIPv4AsyncRangesGenerator(repository.Object);

            var range = await asyncGenerator.GenerateAsync(It.IsAny<IPv4Address>(),
                                                           It.IsAny<IPv4Address>());

            repository.Verify(mock => mock.SaveAsync(default));
        }
    }
}
