﻿using NUnit.Framework;
using OzonTestTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OzonTestTask.Tests.ModelsTests.IPv4AddressTests
{
    [TestFixture]
    public class WhenConvertingToString
    {
        [Test]
        [TestCase(0,    ExpectedResult ="0.0.0.0")]
        [TestCase(1,    ExpectedResult = "0.0.0.1")]
        [TestCase(256,  ExpectedResult = "0.0.1.0")]
        [TestCase(-1,   ExpectedResult = "255.255.255.255")]
        public string ReturnsCorrectString(int ipIntRepresentation)
        {
            var ip = new IPv4Address(ipIntRepresentation);

            return ip.ToString();
        }
    }
}
