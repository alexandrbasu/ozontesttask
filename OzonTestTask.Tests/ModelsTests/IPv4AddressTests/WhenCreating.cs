﻿using NUnit.Framework;
using OzonTestTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OzonTestTask.Tests.ModelsTests.IPv4AddressTests
{
    [TestFixture]
    public class WhenCreating
    {
        [Test]
        [TestCase(0,            ExpectedResult = 0)]
        [TestCase(-1,           ExpectedResult = -1)]
        [TestCase(int.MaxValue, ExpectedResult = int.MaxValue)]
        public int FromIntSetAddressCorrectValue(int ipIntRepresentation)
        {
            var ip = new IPv4Address(ipIntRepresentation);

            return ip.Address;
        }

        [Test]
        [TestCase(0u,            ExpectedResult = 0)]
        [TestCase(uint.MaxValue, ExpectedResult = -1)]
        public int FromUintSetAddressCorrectValue(uint ipUintRepresentation)
        {
            var ip = new IPv4Address(ipUintRepresentation);

            return ip.Address;
        }
    }
}
