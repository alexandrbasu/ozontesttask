﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OzonTestTask.Models;

namespace OzonTestTask.Tests.ModelsTests.IPv4AddressTests
{
    [TestFixture]
    public class WhenParsing
    {
        [Test]
        [TestCase("0.0.0.0",         ExpectedResult = true)]
        [TestCase("255.255.255.255", ExpectedResult = true)]
        [TestCase("1.1.256.1",       ExpectedResult = false)]
        [TestCase("1.1.1",           ExpectedResult = false)]
        [TestCase("1",               ExpectedResult = false)]
        [TestCase("",                ExpectedResult = false)]
        [TestCase(" ",               ExpectedResult = false)]
        [TestCase(null,              ExpectedResult = false)]
        public bool ReturnsTrueIfIpIsCorrect(string ipString)
        {
            var isCorrectIpv4 = IPv4Address.TryParse(ipString, out _);

            return isCorrectIpv4;
        }

        [Test]
        [TestCase("0.0.0.0",         ExpectedResult = 0)]
        [TestCase("255.255.255.255", ExpectedResult = -1)] // -1 = 11111111_11111111_11111111_11111111 
        [TestCase("0.0.0.255",       ExpectedResult = 255)]
        [TestCase("0.0.1.0",         ExpectedResult = 256)]
        public int ReturnsCorrectAddressIntValue(string ipString)
        {
            IPv4Address.TryParse(ipString, out var ipv4);

            return ipv4.Address;
        }


    }
}
