﻿using Newtonsoft.Json;
using OzonTestTask.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OzonTestTask.Models
{
    [JsonConverter(typeof(IPv4AdressConverter))]
    public struct IPv4Address
    {
        public IPv4Address(int ip)
        {
            Address = ip;
        }
        
        public IPv4Address(uint ip)
        {
            Address = unchecked((int)ip);
        }

        public int Address { get; }

        public static bool TryParse(string ipString, out IPv4Address address)
        {
            address = default;

            if (string.IsNullOrWhiteSpace(ipString) || IsValidIPv4Address(ipString) == false)
                return false;

            address = new IPv4Address(IPStringToInt(ipString));

            return true;
        }

        public static IPv4Address Parse(string ipString) 
        {
            if (TryParse(ipString, out var ipv4))
            {
                return ipv4;
            }

            throw new ArgumentException($"Incorrect ip format: \"{ipString}\"");
        }

        public override string ToString()
        {
            #region Constants
            const uint octetMask    = 255; // 0...0_11111111
            const int octetsLength  = 8;
            const int octetsCount   = 4;
            const int IPv4MaxLength = 15;//"255.255.255.255".Length;
            #endregion

            var addressSB = new StringBuilder(IPv4MaxLength);

            var addressCopy = unchecked((uint)Address);

            for (int i = 0; i < octetsCount; i++)
            {
                var octet = addressCopy & octetMask;

                addressSB.Insert(0, '.')
                         .Insert(0, octet);

                addressCopy >>= octetsLength;
            }

            addressSB.Remove(addressSB.Length - 1, 1);

            return addressSB.ToString();
        }

        private static int IPStringToInt(string ipString)
        {
            uint address = 0;

            var ipOctets = ipString.Split('.').Select(o => uint.Parse(o));

            foreach (var octet in ipOctets)
            {
                address <<= 8;
                address += octet;
            }

            return unchecked((int)address);
        }

        private static bool IsValidIPv4Address(string ipString)
        {
            var hasCorrectOctetsCount =
                (ipString.Count(c => c == '.') == 3);

            return hasCorrectOctetsCount &&
                   IPAddress.TryParse(ipString, out _);
        }
    }
}
