﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OzonTestTask.Models.Generators
{
    public interface IIPv4RangesGenerator
    {
        public IEnumerable<IPv4Address> Generate(IPv4Address from, IPv4Address to);
    }
}
