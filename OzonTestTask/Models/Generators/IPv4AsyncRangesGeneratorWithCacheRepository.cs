﻿using Microsoft.EntityFrameworkCore;
using OzonTestTask.Data;
using OzonTestTask.Models.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OzonTestTask.Models.Generators
{
    public class IPv4AsyncRangesGeneratorWithCacheRepository : IIPv4AsyncRangesGenerator
    {
        private readonly IIPv4RangesAsyncRepository rangesRepository;
        private readonly IIPv4RangesGenerator rangesGenerator;

        public IPv4AsyncRangesGeneratorWithCacheRepository(IIPv4RangesAsyncRepository rangesRepository, IIPv4RangesGenerator rangesGenerator)
        {
            this.rangesRepository = rangesRepository;
            this.rangesGenerator  = rangesGenerator;
        }

        public async Task<IEnumerable<IPv4Address>> GenerateAsync(IPv4Address from, IPv4Address to, CancellationToken cancellationToken = default)
        {
            var ipv4Range = await rangesRepository.GetIPv4RangeAsync(from, to, cancellationToken);

            if (ipv4Range == null)
            {
                ipv4Range = new IPv4Range()
                {
                    IpFrom  = from,
                    IpTo    = to,
                    Range   = rangesGenerator.Generate(from, to)
                };
                
                await rangesRepository.InsertIPv4RangeAsync(ipv4Range, cancellationToken);

                try
                {
                    await rangesRepository.SaveAsync(cancellationToken);
                }
                catch (DbUpdateException)
                {
                    // can be logged
                }
            }

            return ipv4Range.Range;
        }
    }
}
