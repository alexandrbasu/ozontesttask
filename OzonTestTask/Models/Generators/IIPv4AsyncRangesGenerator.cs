﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OzonTestTask.Models.Generators
{
    public interface IIPv4AsyncRangesGenerator
    {
        public Task<IEnumerable<IPv4Address>> GenerateAsync(IPv4Address from, IPv4Address to, CancellationToken cancellationToken = default);
    }
}
