﻿using OzonTestTask.Models.Generators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OzonTestTask.Models.Generators
{
    public class IPv4RangesGenerator : IIPv4RangesGenerator
    {
        public IEnumerable<IPv4Address> Generate(IPv4Address from, IPv4Address to)
        {
            var ipFrom  = unchecked((uint)from.Address);
            var ipTo    = unchecked((uint)to.Address);

            for (uint ip = ipFrom; ip <= ipTo; ip++)
            {
                yield return new IPv4Address(ip);
            }
        }
    }
}
