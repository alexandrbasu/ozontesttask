﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OzonTestTask.Models
{
    public class IPv4Range
    {
        public IPv4Address IpFrom { get; set; }
        public IPv4Address IpTo { get; set; }
        
        public IEnumerable<IPv4Address> Range { get; set; }
    }
}
