﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OzonTestTask.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IPv4Ranges",
                columns: table => new
                {
                    IpFrom = table.Column<int>(type: "int", nullable: false),
                    IpTo = table.Column<int>(type: "int", nullable: false),
                    Range = table.Column<string>(type: "varchar(max)", unicode: false, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IPv4Ranges", x => new { x.IpFrom, x.IpTo })
                        .Annotation("SqlServer:Clustered", true);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IPv4Ranges");
        }
    }
}
