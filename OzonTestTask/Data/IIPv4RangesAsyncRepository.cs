﻿using OzonTestTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OzonTestTask.Data
{
    public interface IIPv4RangesAsyncRepository
    {
        Task<IPv4Range> GetIPv4RangeAsync(IPv4Address from, IPv4Address to, CancellationToken cancellationToken = default);
        Task InsertIPv4RangeAsync(IPv4Range range, CancellationToken cancellationToken = default);
        Task SaveAsync(CancellationToken cancellationToken = default);
    }
}
