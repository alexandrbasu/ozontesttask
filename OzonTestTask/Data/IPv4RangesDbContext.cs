﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using OzonTestTask.Converters;
using OzonTestTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OzonTestTask.Data
{
    public class IPv4RangesDbContext : DbContext
    {
        public IPv4RangesDbContext(DbContextOptions<IPv4RangesDbContext> options)
            : base(options)
        {
        }

        public DbSet<IPv4Range> IPv4Ranges { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IPv4Range>()
                        .HasKey(range => new 
                        { 
                            IpFrom  = range.IpFrom, 
                            IpTo    = range.IpTo
                        })
                        .IsClustered();

            modelBuilder.Entity<IPv4Range>()
                        .Property(range => range.IpFrom)
                        .HasConversion(
                            to   => to.Address,
                            from => new IPv4Address(from)
                        );
            
            modelBuilder.Entity<IPv4Range>()
                        .Property(range => range.IpTo)
                        .HasConversion(
                            to   => to.Address,
                            from => new IPv4Address(from)
                        );

            modelBuilder.Entity<IPv4Range>()
                        .Property(range => range.Range)
                        .HasConversion(
                            to   => JsonConvert.SerializeObject(to, new IPv4AdressConverter()),
                            from => JsonConvert.DeserializeObject<IEnumerable<IPv4Address>>(from, new IPv4AdressConverter()))
                        .IsUnicode(false)
                        .IsRequired();
        }
    }
}
