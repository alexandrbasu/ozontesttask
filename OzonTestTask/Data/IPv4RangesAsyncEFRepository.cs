﻿using Microsoft.EntityFrameworkCore;
using OzonTestTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OzonTestTask.Data
{
    public class IPv4RangesAsyncEFRepository : IIPv4RangesAsyncRepository
    {
        private readonly IPv4RangesDbContext dbContext;

        public IPv4RangesAsyncEFRepository(IPv4RangesDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<IPv4Range> GetIPv4RangeAsync(IPv4Address from, IPv4Address to, CancellationToken cancellationToken = default)
        {
            var key = new object[] { from, to };

            // FindAsync version is very slow when range is big
            var ipv4Range = dbContext.IPv4Ranges.Find(key);
            
            return Task.FromResult(ipv4Range);
        }

        public async Task InsertIPv4RangeAsync(IPv4Range range, CancellationToken cancellationToken = default)
        {
            await dbContext.IPv4Ranges.AddAsync(range, cancellationToken);
        }

        public async Task SaveAsync(CancellationToken cancellationToken = default)
        {
            await dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
