﻿using Newtonsoft.Json;
using OzonTestTask.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace OzonTestTask.Converters
{

    public class IPv4AdressConverter : JsonConverter<IPv4Address>
    {
        public override IPv4Address ReadJson(JsonReader reader, Type objectType, [AllowNull] IPv4Address existingValue, bool hasExistingValue, JsonSerializer serializer)
            => IPv4Address.Parse((string)reader.Value);
        

        public override void WriteJson(JsonWriter writer, [AllowNull] IPv4Address value, JsonSerializer serializer)
            => writer.WriteValue(value.ToString());
    }
}
