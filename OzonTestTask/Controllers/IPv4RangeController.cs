﻿using Microsoft.AspNetCore.Mvc;
using OzonTestTask.Models;
using OzonTestTask.Models.Generators;
using OzonTestTask.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OzonTestTask.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class IPv4RangeController : ControllerBase
    {
        private readonly IIPv4AsyncRangesGenerator rangesGenerator;

        public IPv4RangeController(IIPv4AsyncRangesGenerator rangesGenerator)
        {
            this.rangesGenerator = rangesGenerator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> GetIPRange(string from, string to)
        {
            IPv4Address ipFrom  = default, 
                        ipTo    = default;

            var ipsAreValid = IPv4Address.TryParse(from, out ipFrom) && 
                              IPv4Address.TryParse(to, out ipTo);

            if (ipsAreValid == false) 
                return BadRequest(ErrorMessages.InvalidIPv4Format);

            if (IPFromLessOrEqualThanIpTo(ipFrom, ipTo) == false) 
                return BadRequest(ErrorMessages.IPFromIsGreaterThanIpTo);

            var ipsRange = await rangesGenerator.GenerateAsync(ipFrom, ipTo);
            var ipsStringsRange = ipsRange.Select(ip => ip.ToString());

            return Ok(ipsStringsRange);
        }

        [NonAction]
        private bool IPFromLessOrEqualThanIpTo(IPv4Address ipFrom, IPv4Address ipTo) 
        {
            var ipUintFrom  = unchecked((uint)ipFrom.Address);
            var ipUintTo    = unchecked((uint)ipTo.Address);

            return ipUintFrom <= ipUintTo;
        }
    }
}
