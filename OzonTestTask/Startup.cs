using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OzonTestTask.Data;
using OzonTestTask.Models;
using OzonTestTask.Models.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OzonTestTask
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<IPv4RangesDbContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("IPv4RangesConnection")));

            services.AddControllers();
            services.AddScoped<IIPv4RangesAsyncRepository, IPv4RangesAsyncEFRepository>();
            services.AddScoped<IIPv4RangesGenerator, IPv4RangesGenerator>();
            services.AddScoped<IIPv4AsyncRangesGenerator,  IPv4AsyncRangesGeneratorWithCacheRepository>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
